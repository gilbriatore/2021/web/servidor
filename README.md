# Servidor

Orientações para instalação do servidor Node.js.

# Projeto de exemplo MyFood

Vídeo do youtube relacionado ao código:
https://youtu.be/bsqXcgbHw7Y

Playlist do youtube de todas as aulas: 
https://www.youtube.com/watch?v=LvzKuTgo3rg&list=PLfqtDthdyWq4FuAW4b9IzhZVDhflM1PQs

# Para rodar o projeto
0. Instalar no ambiente de desenvolvimento o Node.js (https://nodejs.org).
1. Fazer o checkout do projeto utilizando: git clone https://gitlab.com/gilbriatore/2021/web/servidor.git
2. Rodar, na pasta do servidor, o comando: npm install
3. Executar, na pasta do servidor, o comando: node server.js ou nodemon server.js
4. O servidor será iniciado em `http://localhost:3000/`. 
