const express = require("express");
const path = require("path");

const app = express();  

app.use(express.static(path.join(__dirname, "./static")));

app.get('/', (request, response) => {
  response.sendFile(path.join(__dirname, './static/index.html'));
});

app.listen(8000, () => {
  console.log('Servidor rodando na porta 8000');
});